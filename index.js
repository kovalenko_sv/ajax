class Films {
  constructor(url, entity, root) {
    this.url = url;
    this.entity = entity;
    this.root = root;
  }

  request(url = this.url, entity = "", id = "") {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open("GET", `${url}${entity}${id}`);
      xhr.send();

      xhr.onload = () => resolve(xhr.response);
      xhr.onerror = () => reject(e);
    });
  }

  render() {
    const list = document.createElement("ul");
    this.request(this.url, this.entity)
      .then((response) => {
        const data = JSON.parse(response);
        const items = data.map(
          ({ name, episodeId, openingCrawl, characters }) => {
            const listItem = document.createElement("li");
            const filmName = document.createElement("span");
            listItem.style.cssText =
              "border:1px solid grey; margin:10px;padding:10px; background-color:ivory;border-radius:7px;";

            filmName.innerHTML = `<p style="font-weight:bold">Film name: "${name}"</p>
            <p>Film episode: ${episodeId}</p>
            <p>Short fescription: ${openingCrawl}</p>
            <hr>
            <p style="font-weight:bold">List of film's characters:</p>
            <hr>`;
            listItem.append(filmName);

            characters.forEach((character) => {
              this.request(character).then((response) => {
                const filmCharacters = document.createElement("p");

                const charactersInfo = JSON.parse(response);
                filmCharacters.innerHTML = `${charactersInfo.name}`;
                listItem.append(filmCharacters);
              });
            });
            return listItem;
          }
        );
        list.append(...items);
        this.root.append(list);
      })
      .catch((e) => {
        console.log(e);
      });
  }
}

const BASE_URL = "https://ajax.test-danit.com/api/swapi/";
const entity = "films";
const films = new Films(BASE_URL, entity, root);

films.render();
